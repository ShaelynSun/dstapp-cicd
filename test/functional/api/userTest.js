const chai = require("chai")
const expect = chai.expect
const request = require("supertest")

const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer
const Usr = require("../../../models/usr")

const {MongoClient} = require("mongodb")
let connection, url, db

let server
let mongod
let validID

describe("User", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "dynamicstory" // by default generate random dbName
                }
            })
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            url = await mongod.getConnectionString()
            connection = await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            db = connection.db(await mongod.getDbName())
            server = require("../../../bin/www")
        } catch (error) {
            console.log("err:", error)
        }
    })
    after(async () => {
        try {
            await connection.close()
            await mongod.stop()
        } catch (err) {
            console.log(err)
        }
    })

    beforeEach(async () => {
        try {
            await Usr.deleteMany({})
            let usr = new Usr()
            usr.username = "sxy"
            usr.password = "123"
            await usr.save()
            usr = await Usr.findOne({username: "sxy"})
            validID = usr._id
        } catch (error) {
            console.log(error)
        }
    })

    describe("POST /reg", () => {
        describe("when the user hasn't been existed", () => {
            it("should return confirmation message and user added", () => {
                const usr = {
                    username: "shaelyn",
                    password: "321"
                }
                return request(server)
                    .post("/reg")
                    .send(usr)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Go")
                    })
            })
        })
        describe("when the user has been existed", () => {
            it("should return confirmation message the user not added", () => {
                const usr = {
                    username: "sxy",
                    password: "123"
                }
                return request(server)
                    .post("/reg")
                    .send(usr)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("the user has already existed")
                        expect(res.body.data).to.have.property("username", "sxy")
                    })
            })
        })

    })//end-POST a new user added
    describe("POST /login", () => {
        describe("when the user existed and password is right", () => {
            it("should return confirmation message and login", () => {
                const usr = {
                    username: "sxy",
                    password: "123"
                }
                return request(server)
                    .post("/login")
                    .send(usr)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("login successfully")
                        expect(res.body.data).equals("sxy")
                    })
            })
        })
        describe("when the user existed but password is wrong", () => {
            it("should return message that password is wrong", () => {
                const usr = {
                    username: "sxy",
                    password: "3333333"
                }
                return request(server)
                    .post("/login")
                    .send(usr)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("the password is wrong")
                    })
            })
        })
        describe("when the user not existed", () => {
            it("should return message to advice register", () => {
                const usr = {
                    username: "123",
                    password: "123"
                }
                return request(server)
                    .post("/login")
                    .send(usr)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("the user is not existed")
                    })
            })
        })

    })

})
